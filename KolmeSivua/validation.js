function error () {
    if (!tuote.checkValidity()) {
        document.getElementById("error").innerHTML = "Täytä kaikki kentät!"
        return false;
    } else {
        document.getElementById("error").innerHTML = ""
    }
    
    if (!tarjous.checkValidity()) {
        document.getElementById("error").innerHTML = "Täytä kaikki kentät!"
        return false;
    } else {
        document.getElementById("error").innerHTML = "" 
    }
}
function validointi () {
    let x = document.getElementById("tarjous").value
    if (isNaN(x) || x < 1) {
        document.getElementById("error").innerHTML = "Aikas alhainen hinta teillä!"
        return false;
    } else {
        return true;
    }
}