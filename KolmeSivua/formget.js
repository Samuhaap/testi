window.onload = parametrit;

function parametrit() {
    const queryString = window.location.search;
    console.log(queryString);

    const urlParams = new URLSearchParams(queryString);

    const tuote = urlParams.get('tuote')
    const tarjous = urlParams.get('tarjous')
    const arvonta = urlParams.get('arvonta')

    let tuotenaytto = document.getElementById("tuote")
    let tarjousnaytto = document.getElementById("tarjous")
    let arvontanaytto = document.getElementById("arvonta")

    tuotenaytto.innerHTML = "Valittu tuote: " + tuote
    tarjousnaytto.innerHTML = "Tuotteesta tarjottu hinta: " + tarjous + "€"

    if (arvonta === "YES") {
        arvontanaytto.innerHTML = "Olet myös osallistunut arvontaan"
    }

    if (tuote === null && tarjous === null) {
        tuotenaytto.innerHTML = "Tuote ei valittu"
        tarjousnaytto.innerHTML = "Tarjous ei valittu"
    }
}