function koodi() {
    aika();
    setInterval(aika, 100)
}

function aika(){
fetch('https://worldtimeapi.org/api/timezone/Europe/Helsinki')
    .then(response => response.json())
    .then (data => {
        const datetime = new Date(data.datetime)

        const tunnit = datetime.getHours();
        const minuutit = datetime.getMinutes();
        const sekunnit = datetime.getSeconds();
        const millisekunnit = Math.floor(datetime.getMilliseconds()/100);


        const aika = `Tunnit: ${tunnit} Minuutit: ${minuutit} Sekunnit: ${sekunnit}.${millisekunnit}`;

    document.getElementById("otsikko").innerHTML = aika;
    })
}
