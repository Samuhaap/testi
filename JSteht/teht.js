// 1
let merkkijono = "sana"
let merkkijonom = merkkijono[0].toUpperCase() + merkkijono.slice(1);
console.log(merkkijonom);

// 2
merkkijono = "Apina, Gorilla, Paviaani"
poisto = merkkijono.slice(6,15)
merkkijonom = merkkijono.replace(poisto,'')
console.log(merkkijonom)

// 3
merkkijono = "viides"
merkkijonom = merkkijono.slice(4,5)
console.log(merkkijonom + " on viides kirjain")

// 4, tehty jo 2

// 5
emt(merkkijono, 3)
function emt(a, b) {
    merkkijonom = a.slice(b);
    console.log(merkkijonom)
}

// ehto
let luku = 24

if (luku%3 === 0) {
    console.log("Luku on jaollinen kolmella")
}
if (luku > 10 && luku < 100) {
    console.log("Luku on välillä 10-100")
}

// 
for (let i = 1; i <= 100; i++) {
    if (i%7 === 0) {
        console.log("Luku "+ i +" on jaollinen seitsemällä")
    }
}

