// 0. muodosta lista, joka on 20 elementin pituinen ja sisältää 
// satunnaisia kokonaislukuja välillä 1 - 50

// 1. poista listasta luvut, jotka ovat pienempiä kuin 10 
// ja suurempia kuin 30
 
// 2. plussaa listan joka elementtiin 5 ja tulosta lista 

// 3. etsi listasta ensimmäinen luku, joka on suurempi kuin 20 

// 4. laita lista suuruusjärjestykseen

// 5. tee funktio, joka tarkistaa onko sille annettu string-parametri
// palindromi (eli onko koko sana alusta loppuun sama sana 
// esim. "saippuakauppias")
// vinkki: muuta string ensin arrayksi ja sitten käännä array

// 0
let lista = []
for (let i = 1; i <= 20; i++) {
    let x = Math.floor(Math.random() * (50 - 1) + 1);
    lista.push(x);
}
console.log(lista+" (lista)");
// 1
for (i = lista.length; i != 0; i--) {
    if (lista[i] < 10 || lista[i] > 30) {
        lista.splice(i, 1);
    }
}
console.log(lista+" (10 < lista < 30)");
//2
for (i = 0; i < lista.length; i++) {
    lista[i] += 5;
}
console.log(lista+" kaikkiin lisätty 5");
// 3
for (i = 0; i < lista.length; i++) {
    if (lista[i] > 20) {
        console.log(lista[i]+" oli listan ensimmäinen luku, joka suurempi kuin 20");
        break;
    }
}
// 4
var jlista = new Float64Array(lista);
jlista.sort();
console.log(jlista+" suuruusjärjestyksessä");


// 5
function palindromi(sana) {
    var merkit = sana.split('');
    var kaannetty = merkit.reverse();
    var mKaannetty = kaannetty.join('');
    return sana === mKaannetty;
}

var testiSana = "saippuakauppias";

if (palindromi(testiSana)) {
    console.log(testiSana+" on palindromi.");
} else {
    console.log(testiSana+" ei ole palindromi.");
}

// 8 lista poisto
// lista.splice(0,lista.length)


// 9
let lista1 = [1, 2, 3, 4, 5];
let lista2 = [6, 7, 8, 9, 10];

let yLista = [...lista1.slice(0), "text1", "text2", ...lista2.slice(0)];
// lista1.slice(0,5)
console.log(yLista);

// 10
let initialValue = 0;
let sumWithInitial = lista.reduce((accumulator, currentValue) => accumulator + currentValue, initialValue);
/*
let sumWithInitial = lista.reduce(function (accumulator, currentValue) {
    return accumulator + currentValue;
}, initialValue);
 */
console.log(sumWithInitial+" listan summa");

// 11
class Cat {
    constructor(name, age, owner) {
        this.name = name;
        this.age = age;
        this.owner = owner;
    }
getInfo() {
        return `Name: ${this.name}, age: ${this.age}, owner: ${this.owner}`;
    }
}

const cat1 = new Cat("Vilja", 7, "Pekka");
const cat2 = new Cat("Masa", 4, "Simo");

console.log(cat1.getInfo());
console.log(cat2.getInfo());