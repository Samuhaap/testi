function koodi() {
    fetch('https://dummyjson.com/products/')
    .then(res => res.json())
    .then(json => {
        
        const products = json.products;
        const dataDiv = document.getElementById('data');

        products.forEach(product => {
            const productName = product.title;
            const productPrice = product.price;

            const productInfo = document.createElement('p');
            productInfo.textContent = `Product: ${productName}, Price: $${productPrice}`;

            dataDiv.appendChild(productInfo);
        });
    })
}