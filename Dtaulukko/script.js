window.onload = function() {
  generateTable();
  setPlayerNames();

  $("#nextBtn").click(function() {
    $("body").css("background-color", "blue");
    $(this).css("background-color", "blue");
    $("#resetBtn").css("background-color", "white");
  });

  $("#resetBtn").click(function() {
    $("body").css("background-color", "red");
    $(this).css("background-color", "red");
    $("#nextBtn").css("background-color", "white");
  });
}

function setPlayerNames() {
  const names = $("#names");
  players.plrs.forEach(player => {
    names.append(`<tr><td>${player}</td></tr>`);
  });
}

function generateTable() {
  const table = $("#results");
  players.plrs.forEach((player, i) => {
    const tr = $("<tr></tr>");
    table.append(tr);
    players.plrs.forEach((_, j) => {
      const td = $("<td></td>");
      if (i === j) td.css("background-color", "blue");
      tr.append(td);
    });
  });
}

const players = {};
players.plrs = [
  "Matti Kaisla",
  "Sari Kulmala",
  "Pekka Oikarinen",
  "Timo Kassila",
  "Tiina Oja",
  "Priit Siimar",
  "Janette Koskinen",
  "Rami Harala",
  "Jussi Arola",
  "Jenni Lundgren",
];